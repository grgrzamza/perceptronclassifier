﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;

namespace PerceptronClassifier
{
    public class Perceptron
    {
        private const int BIAS_VALUE = 1;

        private const int COEFFICIENT = 1;

        public int ClassesCount { get; private set; }

        public int VectorSize { get; private set; }

        public List<Vector<double>> FunctionWeights { get; private set; }

        public Perceptron(int classesCount, int vectorSize)
        {
            ClassesCount = classesCount;
            VectorSize = vectorSize;
            FunctionWeights = new List<Vector<double>>(Enumerable.Range(0, classesCount).Select(x => Vector<double>.Build.Dense(vectorSize+1)));
        }

        public void Train(List<Tuple<Vector<double>,int>> trainData)
        {
            bool errors = true;
            while (errors)
            {
                errors = false;
                foreach (Tuple<Vector<double>,int> trainSample in trainData)
                {
                    Vector<double> vector = trainSample.Item1;
                    int expectedClass = trainSample.Item2;
                    vector = AddBias(vector);
                    List<double> functionResults = GetSeparatingFunctionsResults(vector);
                    if (!IsTrueMax(expectedClass, functionResults))
                    {
                        errors = true;
                        Vector<double> adjustment = vector.Multiply(COEFFICIENT);
                        FunctionWeights[expectedClass] = FunctionWeights[expectedClass].Add(adjustment);
                        for (int i = 0; i < FunctionWeights.Count; i++)
                        {
                            if (i != expectedClass)
                            {
                                FunctionWeights[i] = FunctionWeights[i].Subtract(adjustment);
                            }
                        }
                    }
                }
            }
        }

        public int GetClass(Vector<double> vector)
        {
            vector = AddBias(vector);
            List<double> functionResults = GetSeparatingFunctionsResults(vector);
            return functionResults.IndexOf(functionResults.Max());
        }

        private List<double> GetSeparatingFunctionsResults(Vector<double> vector)
        {
            List<double> result = new List<double>();
            foreach(Vector<double> weight in FunctionWeights)
            {
                result.Add(vector.DotProduct(weight));
            }
            return result;
        }

        private Vector<double> AddBias(Vector<double> vector)
        {
            return Vector<double>.Build.DenseOfEnumerable(vector.Concat(new List<double>() { BIAS_VALUE }));
        }

        private bool IsTrueMax(int expectedClass, List<double> functionsResults)
        {
            double expectedMaximum = functionsResults[expectedClass];
            return functionsResults.Where(value => functionsResults.IndexOf(value) != expectedClass).All(value => value < expectedMaximum);
        }
    }
}
